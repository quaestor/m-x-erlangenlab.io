---
title: Lightning Talks - Organizing and Note-Taking with Emacs
date: 2019-06-27
location: Cauerstraße 11
room: 00.131
time: 18:30
description: Lightning talks around the topic of Emacs note-taking
---
For our June meeting, we plan to talk about various topics around organizing and note-taking with Emacs.
The idea is to have multiple short presentations/demos from different speakers on individual topics. So if you have some topic to add, feel free to leave a comment and we'll add it to the schedule.

Schedule:
- Melanie Sigl: [[https://gitlab.com/m-x-erlangen/talks/tree/master/intro-org-mode][Introduction to org-mode]]
- Tobias Langer: [[https://gitlab.com/m-x-erlangen/talks/tree/master/deft][Deft]]
- Merlin Göttlinger: [[https://gitlab.com/m-x-erlangen/talks/tree/master/capture-refile-clocking][Capture, refile, and time tracking with org-mode]]
