---
title: Introduction to Lisp
author: Merlin
date: 2019-05-21
location: Cauerstraße 11
room: 00.131
time: 18:30
description: An introduction to working with Lisp
manual-slides: https://gitlab.com/m-x-erlangen/talks/tree/master/elisp
---
In this meetup we will dive into the world of Lisp.
We want to give a quick introduction to what Lisp is and which functions and features are important when using Emacs. Discussion includes but is not limited to debugging, coding as well as repl.
In the second half of this meeting we want to look a bit into how to configure Emacs.

